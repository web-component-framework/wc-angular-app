import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-doc-viewer',
  templateUrl: './doc-viewer.component.html',
  styleUrls: ['./doc-viewer.component.scss']
})
export class DocViewerComponent implements OnInit {
  doc : string;
  constructor() { }

  ngOnInit() {
    this.doc = `../../assets/pdf/sample.pdf`;
  }

}
