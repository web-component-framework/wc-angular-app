import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ReactiveFormsModule,FormsModule,FormGroup,FormControl,Validators,FormBuilder} from '@angular/forms';
import {HttpService} from '../service/http-service.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  myform: FormGroup; 
  
  constructor(private httpService : HttpService,private router : Router) {
    this.myform = new FormGroup({
      username: new FormControl(null, [Validators.required]),
      password:  new FormControl(null, [Validators.required]),
    });
   }

  ngOnInit() {
  }

  loginUser(){
   localStorage.setItem("username",this.myform.value.username);
   localStorage.setItem("password",this.myform.value.password);
   this.router.navigate(['dashboard']);

  }

}
