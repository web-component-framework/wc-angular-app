import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoughnutGraphPendingComponent } from './doughnut-graph-pending.component';

describe('DoughnutGraphPendingComponent', () => {
  let component: DoughnutGraphPendingComponent;
  let fixture: ComponentFixture<DoughnutGraphPendingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoughnutGraphPendingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoughnutGraphPendingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
