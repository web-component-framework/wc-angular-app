import { Component, OnInit } from "@angular/core";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { TaskService } from "../../app/task-list/task-list.service";
@Component({
  selector: "app-approved",
  templateUrl: "./approved.component.html",
  styleUrls: ["./approved.component.scss"]
})
export class ApprovedComponent implements OnInit {
  confirmed: boolean = false;
  policyId: string;
  taskId: any;
  payload: any;
  constructor(
    private bsModalRef: BsModalRef,
    private taskService: TaskService
  ) {}

  ngOnInit() {}

  confirm() {
    this.taskService.completeTask(this.taskId, this.payload).subscribe(data => {
      console.log(data);
      this.confirmed = true;
    });
  }

  close() {
    this.bsModalRef.hide();
  }

  sendRequest() {
    this.bsModalRef.hide();
  }
}
