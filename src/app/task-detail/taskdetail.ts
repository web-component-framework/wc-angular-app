export const taskDetail = {
	"status": "200",
	"data": {
		"activationTime": "2019-07-17T06:20:03Z",
		"atRiskTime": "2019-07-16T17:32:48Z",
		"clientTypes": ["IBM_WLE_Coach"],
		"completionTime": "2019-07-22T09:47:36Z",
		"containmentContextID": "1223",
		"description": "",
		"displayName": "Step: Junior UW",
		"isAtRisk": true,
		"kind": "KIND_PARTICIPATING",
		"lastModificationTime": "2019-07-22T09:47:36Z",
		"originator": "bpmadmin",
		"priority": 30,
		"startTime": "2019-07-17T06:20:03Z",
		"state": "STATE_FINISHED",
		"piid": "1223",
		"processInstanceName": "Process Policy Main:1223",
		"priorityName": "Normal",
		"data": {
			"variables": {
				"policyDetails": {
					"id": null,
					"contractNumber": "LG111",
					"planName": "Test Plan11",
					"policyStatus": "Submitted",
					"channel": "Bank",
					"remarks": "Please check the tsr and premium_1111",
					"risk": "10000 to 50000",
					"tsr": 1111,
					"premium": 345,
					"document1": null,
					"@metadata": {
						"objectID": "5199d4a5-4585-4a53-8b8d-ea423e896505",
						"dirty": false,
						"invalid": false,
						"shared": false,
						"rootVersionContextID": "2064.8173956a-59dd-4777-9a8b-0d78fe6597d0T",
						"className": "PolicyDetails"
					}
				}
			}
		},
		"externalActivitySnapshotID": null,
		"serviceID": "1.57b6b4a0-7057-402d-9d37-992b45a3d592",
		"serviceSnapshotID": "2064.a6e03f46-e302-4ebf-a353-e1e81e92810b",
		"flowObjectID": "81567835-fab7-4c21-a108-ca4d661fc135",
		"nextTaskId": null,
		"collaboration": {
			"status": false,
			"currentUsers": []
		},
		"actions": null,
		"tkiid": "2363",
		"name": "Junior UW",
		"status": "Closed",
		"owner": "prud_junior_uw1",
		"assignedTo": "prud_junior_uw1",
		"assignedToDisplayName": "Junior Underwriter1",
		"assignedToType": "user",
		"dueTime": "2019-07-17T07:20:03Z"
	}
}