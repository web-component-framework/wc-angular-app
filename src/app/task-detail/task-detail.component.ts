import { Component, OnInit } from "@angular/core";
import { TaskService } from "../task-list/task-list.service";
import { ActivatedRoute } from "@angular/router";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { ApprovedComponent } from "../approved/approved.component";
import { taskDetail } from "./taskdetail";

@Component({
  selector: "app-task-detail",
  templateUrl: "./task-detail.component.html",
  styleUrls: ["./task-detail.component.scss"]
})
export class TaskDetailComponent implements OnInit {
  showSideBar: boolean = false;
  page: string = "dashboard";
  taskId: string;
  tasksDetails: any;
  bsModalRef: BsModalRef;

  constructor(
    private taskService: TaskService,
    private route: ActivatedRoute,
    private modalService: BsModalService
  ) {
    this.taskId = this.route.snapshot.paramMap.get("id");
    this.tasksDetails = {};
  }

  ngOnInit() {
    this.taskService.getTaskDetails(this.taskId).subscribe(data => {
      // console.log(JSON.parse(data.body));
      const _data = data.body;
      this.tasksDetails = _data.data.data.variables;
      console.log(this.tasksDetails);
    });
  }

  toggleMenu() {
    this.showSideBar = !this.showSideBar;
  }

  completeTask() {
    const payload = {
      policyDetails: {
        id: null,
        contractNumber: this.tasksDetails.policyDetails.contractNumber,
        planName: this.tasksDetails.policyDetails.planName,
        policyStatus: this.tasksDetails.policyDetails.policyStatus,
        channel: this.tasksDetails.policyDetails.channel,
        remarks: this.tasksDetails.policyDetails.remarks,
        risk: this.tasksDetails.policyDetails.risk,
        tsr: this.tasksDetails.policyDetails.tsr,
        premium: this.tasksDetails.policyDetails.premium
      }
    };
    console.log(this.taskId);
    console.log(payload);
    const activeModal = this.modalService.show(ApprovedComponent);
    activeModal.content.policyId = this.tasksDetails;
    activeModal.content.taskId = this.taskId;
    activeModal.content.payload = payload;
  }
}
