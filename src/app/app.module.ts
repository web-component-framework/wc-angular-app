import { BrowserModule } from '@angular/platform-browser';
//import { NgModule } from '@angular/core';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SidemenuComponent } from './sidemenu/sidemenu.component';
import { TaskListComponent } from './task-list/task-list.component';
import { TaskDetailComponent } from './task-detail/task-detail.component';
import { PopupComponent } from './popup/popup.component';
import { DoughnutGraphComponent } from './doughnut-graph/doughnut-graph.component';
import { HeaderComponent } from './header/header.component';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { DoughnutGraphPendingComponent } from './doughnut-graph-pending/doughnut-graph-pending.component';
import { DoughnutGraphPriorityComponent } from './doughnut-graph-priority/doughnut-graph-priority.component';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { DocViewerComponent } from './doc-viewer/doc-viewer.component';
import {NgxDocViewerModule} from 'ngx-doc-viewer' 
// import { NgScrollbarModule } from 'ngx-scrollbar';
import { AgGridModule } from 'ag-grid-angular';
import { ModalModule } from 'ngx-bootstrap/modal';


// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ClickableParentComponent } from './clickable/clickable.parent.component';
import { ClickableModule } from './clickable/clickable.module';
import {HttpService} from './service/http-service.service';
import { ApprovedComponent } from './approved/approved.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    // FileViewerComponent,
    SidemenuComponent,
    TaskListComponent,
    TaskDetailComponent,
    PopupComponent,
    DoughnutGraphComponent,
    HeaderComponent,
    DoughnutGraphPendingComponent,
    DoughnutGraphPriorityComponent,
    DocViewerComponent,
    ApprovedComponent,
    
    //LoaderComponent
    //AgGridModule.withComponents([])
  ],
  imports: [
    BrowserModule,
    NgxDocViewerModule,
    CarouselModule.forRoot(),
    ModalModule.forRoot(),
    PopoverModule.forRoot(),
    TabsModule.forRoot(),
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BsDatepickerModule.forRoot(),
    AgGridModule.withComponents([ClickableParentComponent]),
    //sChartsModule,
    AppRoutingModule,
    ClickableModule,
    HttpClientModule

 
    // NgScrollbarModule
  ],
  entryComponents : [ApprovedComponent],
  providers: [HttpService],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
