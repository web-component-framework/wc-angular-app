import { Component, OnInit } from '@angular/core';
import { HostListener, ViewChild } from '@angular/core';
import { BsDatepickerDirective } from 'ngx-bootstrap/datepicker';
import { TaskService } from '../task-list/task-list.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  myTaskData : any[] = [77, 23];
  myTaskLabels : any[] = ['New', 'Old'];
  myTaskBgColors :  any[] = ['#4b7cf3', '#2acd72'];
  pendingData : any[] = [3, 1,6];
  pendingLabels : any[] = ['Additional Documents', 'Medical Reports','Complaince Decisions'];
  pendingBgColors :  any[] = ['#2acd72','#fac751', '#4b7cf3'];
  prorityData : any[] = [11, 7,9];
  prorityLabels : any[] = ['Campaign', 'Channels','Commission'];
  prorityBgColors :  any[] = ['#dd4955', '#2acd72','#fac751'];
  showSideBar :  boolean = true;
  page : string = 'dashboard';
  tasksList : any[] = [];
  constructor(private taskService: TaskService) { }
  

  ngOnInit() {
    this.taskService.getAllTasks().subscribe(data => {
      console.log(data.body.data.items);
      this.tasksList = data.body.data.items;
    });
  }

  toggleMenu(){
    this.showSideBar = !this.showSideBar;
  }

 

}
