import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoughnutGraphPriorityComponent } from './doughnut-graph-priority.component';

describe('DoughnutGraphPriorityComponent', () => {
  let component: DoughnutGraphPriorityComponent;
  let fixture: ComponentFixture<DoughnutGraphPriorityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoughnutGraphPriorityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoughnutGraphPriorityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
