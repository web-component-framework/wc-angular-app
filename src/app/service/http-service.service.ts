import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { environment } from '../../environments/environment';


@Injectable()
export class HttpService {
  constructor(private http: HttpClient) {}

  
    public loginUser(formData){
    return this.http.post(`${environment.apiUrl}/`, formData).map((res: Response) => {
        return res;
    }).catch((error: any) => { return error });
    }


}