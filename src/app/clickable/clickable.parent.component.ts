import {Component} from "@angular/core";
import {ICellRendererAngularComp, AgRendererComponent} from "ag-grid-angular";
import { Router } from '@angular/router';


// both this and the parent component could be folded into one component as they're both simple, but it illustrates how
// a fuller example could work
@Component({
    selector: 'clickable-cell',
    template: `
        <ag-clickable (onClicked)="clicked($event)" [cell]="cell"></ag-clickable>
    `
})
export class ClickableParentComponent implements AgRendererComponent {
    private params: any;
    public cell: any;

    constructor(private router: Router) {}

    agInit(params: any): void {
        this.params = params;
        this.cell = {row: params.data, col: params.colDef.headerName};
        console.log(params);
    }

    public clicked(cell: any): void {
        console.log("Child Cell Clicked: " + JSON.stringify(cell));
        this.router.navigate(['task-detail', cell.row['TASK.TKIID']]);
    }

    refresh(): boolean {
        return false;
    }
}

