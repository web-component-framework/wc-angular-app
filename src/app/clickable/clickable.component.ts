import {Component, EventEmitter, Input, Output, OnInit} from "@angular/core";

@Component({
    selector: 'ag-clickable',
    template: `
        <button style="height: 21px" (click)="click()" class="btn btn-danger">{{isJunior === true ? 'Verify' : 'View Details'}}</button>
        <button style="height: 21px" class="btn btn-default">Reassign</button>
    `,
    styles: [
        `.btn-danger {
            line-height: 0.5;
            background-color: transparent;
            border-color: red;
            color: red;
            font-size: 10px;
        }
        .btn-default {
            margin-left: 10px;
            line-height: 0.5;
            background-color: transparent;
            border-color: grey;
            color: grey;
            font-size: 10px;
        }
        `
    ]
})
export class ClickableComponent implements OnInit {
    isJunior: boolean;
    constructor() {
        this.isJunior = true;
    }
    ngOnInit(): void {
        const isJunior = localStorage.getItem("username").includes("junior");
        console.log("isJunior",isJunior);
    }
    @Input() cell: any;
    @Output() onClicked = new EventEmitter<boolean>();

    click(): void {
        this.onClicked.emit(this.cell);
    }
}