import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.scss']
})
export class SidemenuComponent implements OnInit {
  page : string = 'dashboard';
  
  constructor(private router : Router) { }

  ngOnInit() {
  }
  navigateToPage(page){
    this.page = page;
    this.router.navigate([`/${page}`]);

  }

  logOut(){
    localStorage.clear();
    this.router.navigate(['login']);
  }
}
