import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class TaskService {
    private resourceUrl = 'http://localhost:8083/api';
    // private resourceUrl = '/api';

    constructor(private http: HttpClient) {}

    getAllTasks(): Observable<any> {
        const username = localStorage.getItem("username"), password = localStorage.getItem("password");
        return this.http.get<any[]>(`${this.resourceUrl}/getAllTasks?username=${username}&password=${password}`, { observe: 'response' });
    }

    getTaskDetails(taskId: string): Observable<any> {
        const username = localStorage.getItem("username"), password = localStorage.getItem("password");
        return this.http.get<any>(`${this.resourceUrl}/getTaskDetails?taskId=${taskId}&username=${username}&password=${password}`, { observe: 'response' });
    }

    completeTask(taskId: string, payload: any): Observable<any> {
        const username = localStorage.getItem("username"), password = localStorage.getItem("password");
        return this.http.put<any>(`${this.resourceUrl}/completeTask?taskId=${taskId}&username=${username}&password=${password}`, payload, { observe: 'response' });
    }
}

export const createRequestOption = (req?: any): HttpParams => {
    let options: HttpParams = new HttpParams();
    if (req) {
        Object.keys(req).forEach(key => {
            if (key !== 'sort') {
                options = options.set(key, req[key]);
            }
        });
        if (req.sort) {
            req.sort.forEach(val => {
                options = options.append('sort', val);
            });
        }
    }
    return options;
};