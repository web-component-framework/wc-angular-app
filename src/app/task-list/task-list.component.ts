import { Component, OnInit } from "@angular/core";

import { TaskService } from "./task-list.service";
import { GridOptions } from "ag-grid-community";
import { ClickableParentComponent } from '../clickable/clickable.parent.component';

@Component({
  selector: "app-task-list",
  templateUrl: "./task-list.component.html",
  styleUrls: ["./task-list.component.scss"]
})
export class TaskListComponent implements OnInit {
  public gridOptions: GridOptions;
  showSideBar: boolean;
  rowData: any[];

  constructor(private taskService: TaskService) {
    this.showSideBar = true;
    this.rowData = [];
    this.gridOptions = <GridOptions>{
      rowData: [],
      columnDefs: [
        { headerName: "Contract No.", field: "BDCONTRACT_NUMBER", width: 135, filter: true, sortable: true },
        { headerName: "Plan Name88", field: "BDPLAN_NAME", width: 150 },
        { headerName: "Policy Status", field: "BDPOLICY_STATUS", width: 100 },
        { headerName: "Underwriter", field: "OWNER", width: 100 },
        { headerName: "Channel", field: "BDCHANNEL", width: 100 },
        { headerName: "Risk", field: "BDRISK", width: 100 },
        { headerName: "TSR", field: "BDTSR", width: 100 },
        { headerName: "Premium", field: "BDPREMIUM", width: 80 },
        {
          headerName: "Action",
          field: "TASK.TKIID",
          width: 175,
          suppressSizeToFit: true,
          cellRendererFramework: ClickableParentComponent
        } 
      ],  
      };
    
  }

  
  ngOnInit() {
    this.taskService.getAllTasks().subscribe(data => {
      console.log(data.body.data.items);
      this.gridOptions.rowData = data.body.data.items;
      this.rowData = data.body.data.items;
    });

  }

  page : string = 'tasklist';

  toggleMenu() {
    this.showSideBar = !this.showSideBar;
  }
}
